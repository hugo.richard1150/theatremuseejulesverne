﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpawnPalette : MonoBehaviour
{
    public GameObject instance;
    public bool spawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (spawn)
        {
            spawn = false;

            for (int i = 0; i < 16; i++)
            {
                GameObject go = Instantiate(instance, transform);
                go.transform.localPosition = new Vector3(0.1f * i, 0, 0);
            }
        }
    }
}
