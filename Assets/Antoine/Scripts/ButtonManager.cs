﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public Button[] button;
    public int Delay;

    public void OnClickSecure(Button currentButton)
    {
        for (int i = 0; i < button.Length; i++)
        {
                button[i].GetComponent<Button>().interactable = false;
                StartCoroutine(EnableDelay(button[i]));
          
        }
    }

    IEnumerator EnableDelay(Button button)
    {
        yield return new WaitForSeconds(Delay);
        button.GetComponent<Button>().interactable = true;
    }

    #region Other solution

    /*public void OnClickSecureAll()
    {
        for (int i = 0; i < button.Length; i++)
        {
            button[i].GetComponent<Button>().interactable = false;
            button[i].StartCoroutine(EnableDelayAll());
        }
    }

    IEnumerator EnableDelayAll()
    {
        for (int i = 0; i < button.Length; i++)
        {
            yield return new WaitForSeconds(Delay);
            button[i].GetComponent<Button>().interactable = true;
        }
    }*/

    #endregion
}
