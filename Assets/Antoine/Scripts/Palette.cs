﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Palette : MonoBehaviour
{
    public GameObject palette;
    public string[] allCharact = new string [36] {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",};
    public int rotateCount = 2;

    public void Refresh()
    {
        if (transform.childCount > 0)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }

    public IEnumerator RotatePalette(char charact)
    {
        if(charact != ' ')
        {
            for (int i = 0; i < rotateCount; i++)
            {
                GameObject go = Instantiate(palette, transform);
                go.GetComponentInChildren<Text>(go).text = "X";

                float t = 0;
                while (t < 1)
                {
                    go.transform.localEulerAngles = new Vector3(Mathf.Lerp(0, -180, t), 0, 0);
                    t += Time.deltaTime * 3f;
                    yield return null;
                    Debug.Log(i);
                }
                if (i >= 2)
                {
                    go.GetComponentInChildren<Text>(go).text = charact.ToString();
                    Debug.Log("rotateCount est égale ou supérieur à 0");
                    Debug.Log("found !");
                    go.transform.localEulerAngles = new Vector3(180, 0, 0);
                    break;               
                }
                else
                {
                    if (transform.GetChild(0) != null)
                    {
                        Destroy(transform.GetChild(0).gameObject);
                    }
                }
            }           
        }
    }
}
