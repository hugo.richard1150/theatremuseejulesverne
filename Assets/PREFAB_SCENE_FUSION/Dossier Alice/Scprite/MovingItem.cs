﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingItem : MonoBehaviour
{
    
    [Header("Settings")]
    public LeanTweenType easeType;
    public enum en_Effect {rotate, UpDownLeftRight, Aiguille}
    public en_Effect test;
    public bool pOnce;
    private bool dOnce = true;
    public float delayV;

    [Header("! for Rotate Settings only")]
    public float AngleX = 0;
    public float AngleY = 0;
    public float AngleZ;
    public float RotateSpeed = 1;

    [Header("! for UpDownLeftRight Settings only")]
    public bool usingX;
    public float xMove;
    public float xSpeed;
    public bool usingY;
    public float yMove;
    public float ySpeed;
    public bool usingZ;
    public float zMove;
    public float zSpeed;
    // Update is called once per frame
    void Update()
    {
        if (!dOnce)
            return;
        Debug.Log("Oui");
        switch (test)
        {

            case en_Effect.rotate:
                if (pOnce)
                    dOnce = false;
                
                LeanTween.rotateLocal(gameObject, new Vector3(AngleX,AngleY, AngleZ), RotateSpeed).setDelay(delayV).setLoopPingPong().setEase(easeType);
                break;

            case en_Effect.UpDownLeftRight:
                if (pOnce)
                    dOnce = false;
                if (usingX)
                {
                    LeanTween.moveLocalX(gameObject, xMove, xSpeed).setDelay(delayV).setLoopPingPong().setEase(easeType);
                }

                if (usingY)
                {
                    LeanTween.moveLocalY(gameObject, yMove, ySpeed).setDelay(delayV).setLoopPingPong().setEase(easeType);
                }

                if (usingZ)
                {
                    LeanTween.moveLocalZ(gameObject, zMove, zSpeed).setDelay(delayV).setLoopPingPong().setEase(easeType);
                }

                break;

            case en_Effect.Aiguille:
                if (pOnce)
                    dOnce = false;


                break;
        }

        //LeanTween.moveLocalX(gameObject, 1, 1f).setEaseLinear().setLoopPingPong();
        //LeanTween.moveLocalY(gameObject, 1, 0.5f).setEaseInOutSine().setLoopPingPong();
    }
}
