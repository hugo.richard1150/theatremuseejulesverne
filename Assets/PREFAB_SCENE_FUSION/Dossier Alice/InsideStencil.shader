﻿Shader "Custom/InsideStencil"

{
	Properties
	{
		[NoScaleOffset]_Albedo("Albedo", 2D) = "white" {}
		_TilingX("TilingX", Float) = 1
		_TilingY("TilingY", Float) = 1
		_OffsetX("OffsetX", Float) = 0
		_OffsetY("OffsetY", Float) = 0
		[NoScaleOffset]_NormalMapText("NormalMapText", 2D) = "white" {}
		_NormalStrength("NormalStrength", Float) = 8
		[NoScaleOffset]Texture2D_D5CF32D7("EmissionMap", 2D) = "white" {}
		Color_6C763C74("Emission Color", Color) = (0.5188679, 0.5188679, 0.5188679, 1)
		[NoScaleOffset]Texture2D_33E47080("MetallicMap", 2D) = "white" {}
		Vector1_3F115371("MetallicStrenght", Range(0, 1)) = 1
		Vector1_6029FDBF("Smoothness", Range(0, 1)) = 0
	}
		SubShader
		{
				Tags
				{
					"RenderPipeline" = "UniversalPipeline"
					"RenderType" = "Opaque"
					"Queue" = "Geometry+1"
				}

				Stencil
				{
					Ref 0
					Comp NotEqual
					Pass keep
				}

			Pass
			{
				Name "Universal Forward"
				Tags
				{
					"LightMode" = "UniversalForward"
				}

			// Render State
			Blend One Zero, One Zero
			Cull Off
			ZTest LEqual
			ZWrite On
			// ColorMask: <None>


			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			// Debug
			// <None>

			// --------------------------------------------------
			// Pass

			// Pragmas
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0
			#pragma multi_compile_fog
			#pragma multi_compile_instancing

			// Keywords
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
			// GraphKeywords: <None>

			// Defines
			#define _NORMALMAP 1
			#define _NORMAL_DROPOFF_TS 1
			#define ATTRIBUTES_NEED_NORMAL
			#define ATTRIBUTES_NEED_TANGENT
			#define ATTRIBUTES_NEED_TEXCOORD0
			#define ATTRIBUTES_NEED_TEXCOORD1
			#define VARYINGS_NEED_POSITION_WS 
			#define VARYINGS_NEED_NORMAL_WS
			#define VARYINGS_NEED_TANGENT_WS
			#define VARYINGS_NEED_TEXCOORD0
			#define VARYINGS_NEED_VIEWDIRECTION_WS
			#define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
			#define SHADERPASS_FORWARD

			// Includes
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

			// --------------------------------------------------
			// Graph

			// Graph Properties
			CBUFFER_START(UnityPerMaterial)
			float _TilingX;
			float _TilingY;
			float _OffsetX;
			float _OffsetY;
			float _NormalStrength;
			float4 Color_6C763C74;
			float Vector1_3F115371;
			float Vector1_6029FDBF;
			CBUFFER_END
			TEXTURE2D(_Albedo); SAMPLER(sampler_Albedo); float4 _Albedo_TexelSize;
			TEXTURE2D(_NormalMapText); SAMPLER(sampler_NormalMapText); float4 _NormalMapText_TexelSize;
			TEXTURE2D(Texture2D_D5CF32D7); SAMPLER(samplerTexture2D_D5CF32D7); float4 Texture2D_D5CF32D7_TexelSize;
			TEXTURE2D(Texture2D_33E47080); SAMPLER(samplerTexture2D_33E47080); float4 Texture2D_33E47080_TexelSize;
			SAMPLER(_SampleTexture2D_4F2A6AC5_Sampler_3_Linear_Repeat);
			SAMPLER(_NormalFromTexture_B89CB9C6_Sampler_2_Linear_Repeat);
			SAMPLER(_SampleTexture2D_F60AE000_Sampler_3_Linear_Repeat);
			SAMPLER(_SampleTexture2D_BC4A4F41_Sampler_3_Linear_Repeat);

			// Graph Functions

			void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
			{
				RGBA = float4(R, G, B, A);
				RGB = float3(R, G, B);
				RG = float2(R, G);
			}

			void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
			{
				Out = UV * Tiling + Offset;
			}

			void Unity_NormalFromTexture_float(Texture2D Texture, SamplerState Sampler, float2 UV, float Offset, float Strength, out float3 Out)
			{
				Offset = pow(Offset, 3) * 0.1;
				float2 offsetU = float2(UV.x + Offset, UV.y);
				float2 offsetV = float2(UV.x, UV.y + Offset);
				float normalSample = Texture.Sample(Sampler, UV);
				float uSample = Texture.Sample(Sampler, offsetU);
				float vSample = Texture.Sample(Sampler, offsetV);
				float3 va = float3(1, 0, (uSample - normalSample) * Strength);
				float3 vb = float3(0, 1, (vSample - normalSample) * Strength);
				Out = normalize(cross(va, vb));
			}

			void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
			{
				Out = A * B;
			}

			// Graph Vertex
			// GraphVertex: <None>

			// Graph Pixel
			struct SurfaceDescriptionInputs
			{
				float4 uv0;
			};

			struct SurfaceDescription
			{
				float3 Albedo;
				float3 Normal;
				float3 Emission;
				float Metallic;
				float Smoothness;
				float Occlusion;
				float Alpha;
				float AlphaClipThreshold;
			};

			SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
			{
				SurfaceDescription surface = (SurfaceDescription)0;
				float _Property_6AB104D3_Out_0 = _TilingX;
				float _Property_D3E9070B_Out_0 = _TilingY;
				float4 _Combine_15D4F6D7_RGBA_4;
				float3 _Combine_15D4F6D7_RGB_5;
				float2 _Combine_15D4F6D7_RG_6;
				Unity_Combine_float(_Property_6AB104D3_Out_0, _Property_D3E9070B_Out_0, 0, 0, _Combine_15D4F6D7_RGBA_4, _Combine_15D4F6D7_RGB_5, _Combine_15D4F6D7_RG_6);
				float _Property_C5EE2266_Out_0 = _OffsetX;
				float _Property_3C3E4A46_Out_0 = _OffsetY;
				float4 _Combine_2E4A8FFE_RGBA_4;
				float3 _Combine_2E4A8FFE_RGB_5;
				float2 _Combine_2E4A8FFE_RG_6;
				Unity_Combine_float(_Property_C5EE2266_Out_0, _Property_3C3E4A46_Out_0, 0, 0, _Combine_2E4A8FFE_RGBA_4, _Combine_2E4A8FFE_RGB_5, _Combine_2E4A8FFE_RG_6);
				float2 _TilingAndOffset_B1CFF5A4_Out_3;
				Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_15D4F6D7_RG_6, _Combine_2E4A8FFE_RG_6, _TilingAndOffset_B1CFF5A4_Out_3);
				float4 _SampleTexture2D_4F2A6AC5_RGBA_0 = SAMPLE_TEXTURE2D(_Albedo, sampler_Albedo, _TilingAndOffset_B1CFF5A4_Out_3);
				float _SampleTexture2D_4F2A6AC5_R_4 = _SampleTexture2D_4F2A6AC5_RGBA_0.r;
				float _SampleTexture2D_4F2A6AC5_G_5 = _SampleTexture2D_4F2A6AC5_RGBA_0.g;
				float _SampleTexture2D_4F2A6AC5_B_6 = _SampleTexture2D_4F2A6AC5_RGBA_0.b;
				float _SampleTexture2D_4F2A6AC5_A_7 = _SampleTexture2D_4F2A6AC5_RGBA_0.a;
				float _Property_D734D4DB_Out_0 = _TilingX;
				float _Property_4167A4FD_Out_0 = _TilingY;
				float4 _Combine_19DF5355_RGBA_4;
				float3 _Combine_19DF5355_RGB_5;
				float2 _Combine_19DF5355_RG_6;
				Unity_Combine_float(_Property_D734D4DB_Out_0, _Property_4167A4FD_Out_0, 0, 0, _Combine_19DF5355_RGBA_4, _Combine_19DF5355_RGB_5, _Combine_19DF5355_RG_6);
				float _Property_E8B2929D_Out_0 = _OffsetX;
				float _Property_2A2B8308_Out_0 = _OffsetY;
				float4 _Combine_76A95B71_RGBA_4;
				float3 _Combine_76A95B71_RGB_5;
				float2 _Combine_76A95B71_RG_6;
				Unity_Combine_float(_Property_E8B2929D_Out_0, _Property_2A2B8308_Out_0, 0, 0, _Combine_76A95B71_RGBA_4, _Combine_76A95B71_RGB_5, _Combine_76A95B71_RG_6);
				float2 _TilingAndOffset_837518D3_Out_3;
				Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_19DF5355_RG_6, _Combine_76A95B71_RG_6, _TilingAndOffset_837518D3_Out_3);
				float _Property_1A163D55_Out_0 = _NormalStrength;
				float3 _NormalFromTexture_B89CB9C6_Out_5;
				Unity_NormalFromTexture_float(_NormalMapText, sampler_NormalMapText, _TilingAndOffset_837518D3_Out_3, 0.5, _Property_1A163D55_Out_0, _NormalFromTexture_B89CB9C6_Out_5);
				float _Property_CDAEE282_Out_0 = _TilingX;
				float _Property_2D7577F6_Out_0 = _TilingY;
				float4 _Combine_585809E_RGBA_4;
				float3 _Combine_585809E_RGB_5;
				float2 _Combine_585809E_RG_6;
				Unity_Combine_float(_Property_CDAEE282_Out_0, _Property_2D7577F6_Out_0, 0, 0, _Combine_585809E_RGBA_4, _Combine_585809E_RGB_5, _Combine_585809E_RG_6);
				float _Property_3D74E4D0_Out_0 = _OffsetX;
				float _Property_5CADE9BC_Out_0 = _OffsetY;
				float4 _Combine_791B0E67_RGBA_4;
				float3 _Combine_791B0E67_RGB_5;
				float2 _Combine_791B0E67_RG_6;
				Unity_Combine_float(_Property_3D74E4D0_Out_0, _Property_5CADE9BC_Out_0, 0, 0, _Combine_791B0E67_RGBA_4, _Combine_791B0E67_RGB_5, _Combine_791B0E67_RG_6);
				float2 _TilingAndOffset_3D772835_Out_3;
				Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_585809E_RG_6, _Combine_791B0E67_RG_6, _TilingAndOffset_3D772835_Out_3);
				float4 _SampleTexture2D_F60AE000_RGBA_0 = SAMPLE_TEXTURE2D(Texture2D_D5CF32D7, samplerTexture2D_D5CF32D7, _TilingAndOffset_3D772835_Out_3);
				float _SampleTexture2D_F60AE000_R_4 = _SampleTexture2D_F60AE000_RGBA_0.r;
				float _SampleTexture2D_F60AE000_G_5 = _SampleTexture2D_F60AE000_RGBA_0.g;
				float _SampleTexture2D_F60AE000_B_6 = _SampleTexture2D_F60AE000_RGBA_0.b;
				float _SampleTexture2D_F60AE000_A_7 = _SampleTexture2D_F60AE000_RGBA_0.a;
				float4 _Property_51357914_Out_0 = Color_6C763C74;
				float4 _Multiply_67569EC5_Out_2;
				Unity_Multiply_float(_SampleTexture2D_F60AE000_RGBA_0, _Property_51357914_Out_0, _Multiply_67569EC5_Out_2);
				float _Property_E6D0C368_Out_0 = _TilingX;
				float _Property_52E39EF4_Out_0 = _TilingY;
				float4 _Combine_A6A69A2D_RGBA_4;
				float3 _Combine_A6A69A2D_RGB_5;
				float2 _Combine_A6A69A2D_RG_6;
				Unity_Combine_float(_Property_E6D0C368_Out_0, _Property_52E39EF4_Out_0, 0, 0, _Combine_A6A69A2D_RGBA_4, _Combine_A6A69A2D_RGB_5, _Combine_A6A69A2D_RG_6);
				float _Property_A9E1A6BA_Out_0 = _OffsetX;
				float _Property_33109A43_Out_0 = _OffsetY;
				float4 _Combine_842DDA47_RGBA_4;
				float3 _Combine_842DDA47_RGB_5;
				float2 _Combine_842DDA47_RG_6;
				Unity_Combine_float(_Property_A9E1A6BA_Out_0, _Property_33109A43_Out_0, 0, 0, _Combine_842DDA47_RGBA_4, _Combine_842DDA47_RGB_5, _Combine_842DDA47_RG_6);
				float2 _TilingAndOffset_8920D11A_Out_3;
				Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_A6A69A2D_RG_6, _Combine_842DDA47_RG_6, _TilingAndOffset_8920D11A_Out_3);
				float4 _SampleTexture2D_BC4A4F41_RGBA_0 = SAMPLE_TEXTURE2D(Texture2D_33E47080, samplerTexture2D_33E47080, _TilingAndOffset_8920D11A_Out_3);
				float _SampleTexture2D_BC4A4F41_R_4 = _SampleTexture2D_BC4A4F41_RGBA_0.r;
				float _SampleTexture2D_BC4A4F41_G_5 = _SampleTexture2D_BC4A4F41_RGBA_0.g;
				float _SampleTexture2D_BC4A4F41_B_6 = _SampleTexture2D_BC4A4F41_RGBA_0.b;
				float _SampleTexture2D_BC4A4F41_A_7 = _SampleTexture2D_BC4A4F41_RGBA_0.a;
				float _Property_B2DDEBDA_Out_0 = Vector1_3F115371;
				float4 _Multiply_4AD72BC2_Out_2;
				Unity_Multiply_float(_SampleTexture2D_BC4A4F41_RGBA_0, (_Property_B2DDEBDA_Out_0.xxxx), _Multiply_4AD72BC2_Out_2);
				float _Property_328676B3_Out_0 = Vector1_6029FDBF;
				surface.Albedo = (_SampleTexture2D_4F2A6AC5_RGBA_0.xyz);
				surface.Normal = _NormalFromTexture_B89CB9C6_Out_5;
				surface.Emission = (_Multiply_67569EC5_Out_2.xyz);
				surface.Metallic = (_Multiply_4AD72BC2_Out_2).x;
				surface.Smoothness = _Property_328676B3_Out_0;
				surface.Occlusion = 1;
				surface.Alpha = 1;
				surface.AlphaClipThreshold = 0;
				return surface;
			}

			// --------------------------------------------------
			// Structs and Packing

			// Generated Type: Attributes
			struct Attributes
			{
				float3 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float4 uv0 : TEXCOORD0;
				float4 uv1 : TEXCOORD1;
				#if UNITY_ANY_INSTANCING_ENABLED
				uint instanceID : INSTANCEID_SEMANTIC;
				#endif
			};

			// Generated Type: Varyings
			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float3 positionWS;
				float3 normalWS;
				float4 tangentWS;
				float4 texCoord0;
				float3 viewDirectionWS;
				#if defined(LIGHTMAP_ON)
				float2 lightmapUV;
				#endif
				#if !defined(LIGHTMAP_ON)
				float3 sh;
				#endif
				float4 fogFactorAndVertexLight;
				float4 shadowCoord;
				#if UNITY_ANY_INSTANCING_ENABLED
				uint instanceID : CUSTOM_INSTANCE_ID;
				#endif
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
				#endif
			};

			// Generated Type: PackedVaryings
			struct PackedVaryings
			{
				float4 positionCS : SV_POSITION;
				#if defined(LIGHTMAP_ON)
				#endif
				#if !defined(LIGHTMAP_ON)
				#endif
				#if UNITY_ANY_INSTANCING_ENABLED
				uint instanceID : CUSTOM_INSTANCE_ID;
				#endif
				float3 interp00 : TEXCOORD0;
				float3 interp01 : TEXCOORD1;
				float4 interp02 : TEXCOORD2;
				float4 interp03 : TEXCOORD3;
				float3 interp04 : TEXCOORD4;
				float2 interp05 : TEXCOORD5;
				float3 interp06 : TEXCOORD6;
				float4 interp07 : TEXCOORD7;
				float4 interp08 : TEXCOORD8;
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
				#endif
			};

			// Packed Type: Varyings
			PackedVaryings PackVaryings(Varyings input)
			{
				PackedVaryings output = (PackedVaryings)0;
				output.positionCS = input.positionCS;
				output.interp00.xyz = input.positionWS;
				output.interp01.xyz = input.normalWS;
				output.interp02.xyzw = input.tangentWS;
				output.interp03.xyzw = input.texCoord0;
				output.interp04.xyz = input.viewDirectionWS;
				#if defined(LIGHTMAP_ON)
				output.interp05.xy = input.lightmapUV;
				#endif
				#if !defined(LIGHTMAP_ON)
				output.interp06.xyz = input.sh;
				#endif
				output.interp07.xyzw = input.fogFactorAndVertexLight;
				output.interp08.xyzw = input.shadowCoord;
				#if UNITY_ANY_INSTANCING_ENABLED
				output.instanceID = input.instanceID;
				#endif
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				output.cullFace = input.cullFace;
				#endif
				return output;
			}

			// Unpacked Type: Varyings
			Varyings UnpackVaryings(PackedVaryings input)
			{
				Varyings output = (Varyings)0;
				output.positionCS = input.positionCS;
				output.positionWS = input.interp00.xyz;
				output.normalWS = input.interp01.xyz;
				output.tangentWS = input.interp02.xyzw;
				output.texCoord0 = input.interp03.xyzw;
				output.viewDirectionWS = input.interp04.xyz;
				#if defined(LIGHTMAP_ON)
				output.lightmapUV = input.interp05.xy;
				#endif
				#if !defined(LIGHTMAP_ON)
				output.sh = input.interp06.xyz;
				#endif
				output.fogFactorAndVertexLight = input.interp07.xyzw;
				output.shadowCoord = input.interp08.xyzw;
				#if UNITY_ANY_INSTANCING_ENABLED
				output.instanceID = input.instanceID;
				#endif
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				output.cullFace = input.cullFace;
				#endif
				return output;
			}

			// --------------------------------------------------
			// Build Graph Inputs

			SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
			{
				SurfaceDescriptionInputs output;
				ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





				output.uv0 = input.texCoord0;
			#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
			#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
			#else
			#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
			#endif
			#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

				return output;
			}


			// --------------------------------------------------
			// Main

			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags
			{
				"LightMode" = "ShadowCaster"
			}

				// Render State
				Blend One Zero, One Zero
				Cull Off
				ZTest LEqual
				ZWrite On
				// ColorMask: <None>


				HLSLPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				// Debug
				// <None>

				// --------------------------------------------------
				// Pass

				// Pragmas
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x
				#pragma target 2.0
				#pragma multi_compile_instancing

				// Keywords
				// PassKeywords: <None>
				// GraphKeywords: <None>

				// Defines
				#define _NORMALMAP 1
				#define _NORMAL_DROPOFF_TS 1
				#define ATTRIBUTES_NEED_NORMAL
				#define ATTRIBUTES_NEED_TANGENT
				#define SHADERPASS_SHADOWCASTER

				// Includes
				#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
				#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

				// --------------------------------------------------
				// Graph

				// Graph Properties
				CBUFFER_START(UnityPerMaterial)
				float _TilingX;
				float _TilingY;
				float _OffsetX;
				float _OffsetY;
				float _NormalStrength;
				float4 Color_6C763C74;
				float Vector1_3F115371;
				float Vector1_6029FDBF;
				CBUFFER_END
				TEXTURE2D(_Albedo); SAMPLER(sampler_Albedo); float4 _Albedo_TexelSize;
				TEXTURE2D(_NormalMapText); SAMPLER(sampler_NormalMapText); float4 _NormalMapText_TexelSize;
				TEXTURE2D(Texture2D_D5CF32D7); SAMPLER(samplerTexture2D_D5CF32D7); float4 Texture2D_D5CF32D7_TexelSize;
				TEXTURE2D(Texture2D_33E47080); SAMPLER(samplerTexture2D_33E47080); float4 Texture2D_33E47080_TexelSize;

				// Graph Functions
				// GraphFunctions: <None>

				// Graph Vertex
				// GraphVertex: <None>

				// Graph Pixel
				struct SurfaceDescriptionInputs
				{
				};

				struct SurfaceDescription
				{
					float Alpha;
					float AlphaClipThreshold;
				};

				SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
				{
					SurfaceDescription surface = (SurfaceDescription)0;
					surface.Alpha = 1;
					surface.AlphaClipThreshold = 0;
					return surface;
				}

				// --------------------------------------------------
				// Structs and Packing

				// Generated Type: Attributes
				struct Attributes
				{
					float3 positionOS : POSITION;
					float3 normalOS : NORMAL;
					float4 tangentOS : TANGENT;
					#if UNITY_ANY_INSTANCING_ENABLED
					uint instanceID : INSTANCEID_SEMANTIC;
					#endif
				};

				// Generated Type: Varyings
				struct Varyings
				{
					float4 positionCS : SV_POSITION;
					#if UNITY_ANY_INSTANCING_ENABLED
					uint instanceID : CUSTOM_INSTANCE_ID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
					#endif
				};

				// Generated Type: PackedVaryings
				struct PackedVaryings
				{
					float4 positionCS : SV_POSITION;
					#if UNITY_ANY_INSTANCING_ENABLED
					uint instanceID : CUSTOM_INSTANCE_ID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
					#endif
				};

				// Packed Type: Varyings
				PackedVaryings PackVaryings(Varyings input)
				{
					PackedVaryings output = (PackedVaryings)0;
					output.positionCS = input.positionCS;
					#if UNITY_ANY_INSTANCING_ENABLED
					output.instanceID = input.instanceID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					output.cullFace = input.cullFace;
					#endif
					return output;
				}

				// Unpacked Type: Varyings
				Varyings UnpackVaryings(PackedVaryings input)
				{
					Varyings output = (Varyings)0;
					output.positionCS = input.positionCS;
					#if UNITY_ANY_INSTANCING_ENABLED
					output.instanceID = input.instanceID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					output.cullFace = input.cullFace;
					#endif
					return output;
				}

				// --------------------------------------------------
				// Build Graph Inputs

				SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
				{
					SurfaceDescriptionInputs output;
					ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
				#else
				#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
				#endif
				#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

					return output;
				}


				// --------------------------------------------------
				// Main

				#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

				ENDHLSL
			}

			Pass
			{
				Name "DepthOnly"
				Tags
				{
					"LightMode" = "DepthOnly"
				}

					// Render State
					Blend One Zero, One Zero
					Cull Off
					ZTest LEqual
					ZWrite On
					ColorMask 0


					HLSLPROGRAM
					#pragma vertex vert
					#pragma fragment frag

					// Debug
					// <None>

					// --------------------------------------------------
					// Pass

					// Pragmas
					#pragma prefer_hlslcc gles
					#pragma exclude_renderers d3d11_9x
					#pragma target 2.0
					#pragma multi_compile_instancing

					// Keywords
					// PassKeywords: <None>
					// GraphKeywords: <None>

					// Defines
					#define _NORMALMAP 1
					#define _NORMAL_DROPOFF_TS 1
					#define ATTRIBUTES_NEED_NORMAL
					#define ATTRIBUTES_NEED_TANGENT
					#define SHADERPASS_DEPTHONLY

					// Includes
					#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
					#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

					// --------------------------------------------------
					// Graph

					// Graph Properties
					CBUFFER_START(UnityPerMaterial)
					float _TilingX;
					float _TilingY;
					float _OffsetX;
					float _OffsetY;
					float _NormalStrength;
					float4 Color_6C763C74;
					float Vector1_3F115371;
					float Vector1_6029FDBF;
					CBUFFER_END
					TEXTURE2D(_Albedo); SAMPLER(sampler_Albedo); float4 _Albedo_TexelSize;
					TEXTURE2D(_NormalMapText); SAMPLER(sampler_NormalMapText); float4 _NormalMapText_TexelSize;
					TEXTURE2D(Texture2D_D5CF32D7); SAMPLER(samplerTexture2D_D5CF32D7); float4 Texture2D_D5CF32D7_TexelSize;
					TEXTURE2D(Texture2D_33E47080); SAMPLER(samplerTexture2D_33E47080); float4 Texture2D_33E47080_TexelSize;

					// Graph Functions
					// GraphFunctions: <None>

					// Graph Vertex
					// GraphVertex: <None>

					// Graph Pixel
					struct SurfaceDescriptionInputs
					{
					};

					struct SurfaceDescription
					{
						float Alpha;
						float AlphaClipThreshold;
					};

					SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
					{
						SurfaceDescription surface = (SurfaceDescription)0;
						surface.Alpha = 1;
						surface.AlphaClipThreshold = 0;
						return surface;
					}

					// --------------------------------------------------
					// Structs and Packing

					// Generated Type: Attributes
					struct Attributes
					{
						float3 positionOS : POSITION;
						float3 normalOS : NORMAL;
						float4 tangentOS : TANGENT;
						#if UNITY_ANY_INSTANCING_ENABLED
						uint instanceID : INSTANCEID_SEMANTIC;
						#endif
					};

					// Generated Type: Varyings
					struct Varyings
					{
						float4 positionCS : SV_POSITION;
						#if UNITY_ANY_INSTANCING_ENABLED
						uint instanceID : CUSTOM_INSTANCE_ID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
						#endif
					};

					// Generated Type: PackedVaryings
					struct PackedVaryings
					{
						float4 positionCS : SV_POSITION;
						#if UNITY_ANY_INSTANCING_ENABLED
						uint instanceID : CUSTOM_INSTANCE_ID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
						#endif
					};

					// Packed Type: Varyings
					PackedVaryings PackVaryings(Varyings input)
					{
						PackedVaryings output = (PackedVaryings)0;
						output.positionCS = input.positionCS;
						#if UNITY_ANY_INSTANCING_ENABLED
						output.instanceID = input.instanceID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						output.cullFace = input.cullFace;
						#endif
						return output;
					}

					// Unpacked Type: Varyings
					Varyings UnpackVaryings(PackedVaryings input)
					{
						Varyings output = (Varyings)0;
						output.positionCS = input.positionCS;
						#if UNITY_ANY_INSTANCING_ENABLED
						output.instanceID = input.instanceID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						output.cullFace = input.cullFace;
						#endif
						return output;
					}

					// --------------------------------------------------
					// Build Graph Inputs

					SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
					{
						SurfaceDescriptionInputs output;
						ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
					#else
					#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
					#endif
					#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

						return output;
					}


					// --------------------------------------------------
					// Main

					#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

					ENDHLSL
				}

				Pass
				{
					Name "Meta"
					Tags
					{
						"LightMode" = "Meta"
					}

						// Render State
						Blend One Zero, One Zero
						Cull Off
						ZTest LEqual
						ZWrite On
						// ColorMask: <None>


						HLSLPROGRAM
						#pragma vertex vert
						#pragma fragment frag

						// Debug
						// <None>

						// --------------------------------------------------
						// Pass

						// Pragmas
						#pragma prefer_hlslcc gles
						#pragma exclude_renderers d3d11_9x
						#pragma target 2.0

						// Keywords
						#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
						// GraphKeywords: <None>

						// Defines
						#define _NORMALMAP 1
						#define _NORMAL_DROPOFF_TS 1
						#define ATTRIBUTES_NEED_NORMAL
						#define ATTRIBUTES_NEED_TANGENT
						#define ATTRIBUTES_NEED_TEXCOORD0
						#define ATTRIBUTES_NEED_TEXCOORD1
						#define ATTRIBUTES_NEED_TEXCOORD2
						#define VARYINGS_NEED_TEXCOORD0
						#define SHADERPASS_META

						// Includes
						#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
						#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
						#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
						#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
						#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
						#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

						// --------------------------------------------------
						// Graph

						// Graph Properties
						CBUFFER_START(UnityPerMaterial)
						float _TilingX;
						float _TilingY;
						float _OffsetX;
						float _OffsetY;
						float _NormalStrength;
						float4 Color_6C763C74;
						float Vector1_3F115371;
						float Vector1_6029FDBF;
						CBUFFER_END
						TEXTURE2D(_Albedo); SAMPLER(sampler_Albedo); float4 _Albedo_TexelSize;
						TEXTURE2D(_NormalMapText); SAMPLER(sampler_NormalMapText); float4 _NormalMapText_TexelSize;
						TEXTURE2D(Texture2D_D5CF32D7); SAMPLER(samplerTexture2D_D5CF32D7); float4 Texture2D_D5CF32D7_TexelSize;
						TEXTURE2D(Texture2D_33E47080); SAMPLER(samplerTexture2D_33E47080); float4 Texture2D_33E47080_TexelSize;
						SAMPLER(_SampleTexture2D_4F2A6AC5_Sampler_3_Linear_Repeat);
						SAMPLER(_SampleTexture2D_F60AE000_Sampler_3_Linear_Repeat);

						// Graph Functions

						void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
						{
							RGBA = float4(R, G, B, A);
							RGB = float3(R, G, B);
							RG = float2(R, G);
						}

						void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
						{
							Out = UV * Tiling + Offset;
						}

						void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
						{
							Out = A * B;
						}

						// Graph Vertex
						// GraphVertex: <None>

						// Graph Pixel
						struct SurfaceDescriptionInputs
						{
							float4 uv0;
						};

						struct SurfaceDescription
						{
							float3 Albedo;
							float3 Emission;
							float Alpha;
							float AlphaClipThreshold;
						};

						SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
						{
							SurfaceDescription surface = (SurfaceDescription)0;
							float _Property_6AB104D3_Out_0 = _TilingX;
							float _Property_D3E9070B_Out_0 = _TilingY;
							float4 _Combine_15D4F6D7_RGBA_4;
							float3 _Combine_15D4F6D7_RGB_5;
							float2 _Combine_15D4F6D7_RG_6;
							Unity_Combine_float(_Property_6AB104D3_Out_0, _Property_D3E9070B_Out_0, 0, 0, _Combine_15D4F6D7_RGBA_4, _Combine_15D4F6D7_RGB_5, _Combine_15D4F6D7_RG_6);
							float _Property_C5EE2266_Out_0 = _OffsetX;
							float _Property_3C3E4A46_Out_0 = _OffsetY;
							float4 _Combine_2E4A8FFE_RGBA_4;
							float3 _Combine_2E4A8FFE_RGB_5;
							float2 _Combine_2E4A8FFE_RG_6;
							Unity_Combine_float(_Property_C5EE2266_Out_0, _Property_3C3E4A46_Out_0, 0, 0, _Combine_2E4A8FFE_RGBA_4, _Combine_2E4A8FFE_RGB_5, _Combine_2E4A8FFE_RG_6);
							float2 _TilingAndOffset_B1CFF5A4_Out_3;
							Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_15D4F6D7_RG_6, _Combine_2E4A8FFE_RG_6, _TilingAndOffset_B1CFF5A4_Out_3);
							float4 _SampleTexture2D_4F2A6AC5_RGBA_0 = SAMPLE_TEXTURE2D(_Albedo, sampler_Albedo, _TilingAndOffset_B1CFF5A4_Out_3);
							float _SampleTexture2D_4F2A6AC5_R_4 = _SampleTexture2D_4F2A6AC5_RGBA_0.r;
							float _SampleTexture2D_4F2A6AC5_G_5 = _SampleTexture2D_4F2A6AC5_RGBA_0.g;
							float _SampleTexture2D_4F2A6AC5_B_6 = _SampleTexture2D_4F2A6AC5_RGBA_0.b;
							float _SampleTexture2D_4F2A6AC5_A_7 = _SampleTexture2D_4F2A6AC5_RGBA_0.a;
							float _Property_CDAEE282_Out_0 = _TilingX;
							float _Property_2D7577F6_Out_0 = _TilingY;
							float4 _Combine_585809E_RGBA_4;
							float3 _Combine_585809E_RGB_5;
							float2 _Combine_585809E_RG_6;
							Unity_Combine_float(_Property_CDAEE282_Out_0, _Property_2D7577F6_Out_0, 0, 0, _Combine_585809E_RGBA_4, _Combine_585809E_RGB_5, _Combine_585809E_RG_6);
							float _Property_3D74E4D0_Out_0 = _OffsetX;
							float _Property_5CADE9BC_Out_0 = _OffsetY;
							float4 _Combine_791B0E67_RGBA_4;
							float3 _Combine_791B0E67_RGB_5;
							float2 _Combine_791B0E67_RG_6;
							Unity_Combine_float(_Property_3D74E4D0_Out_0, _Property_5CADE9BC_Out_0, 0, 0, _Combine_791B0E67_RGBA_4, _Combine_791B0E67_RGB_5, _Combine_791B0E67_RG_6);
							float2 _TilingAndOffset_3D772835_Out_3;
							Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_585809E_RG_6, _Combine_791B0E67_RG_6, _TilingAndOffset_3D772835_Out_3);
							float4 _SampleTexture2D_F60AE000_RGBA_0 = SAMPLE_TEXTURE2D(Texture2D_D5CF32D7, samplerTexture2D_D5CF32D7, _TilingAndOffset_3D772835_Out_3);
							float _SampleTexture2D_F60AE000_R_4 = _SampleTexture2D_F60AE000_RGBA_0.r;
							float _SampleTexture2D_F60AE000_G_5 = _SampleTexture2D_F60AE000_RGBA_0.g;
							float _SampleTexture2D_F60AE000_B_6 = _SampleTexture2D_F60AE000_RGBA_0.b;
							float _SampleTexture2D_F60AE000_A_7 = _SampleTexture2D_F60AE000_RGBA_0.a;
							float4 _Property_51357914_Out_0 = Color_6C763C74;
							float4 _Multiply_67569EC5_Out_2;
							Unity_Multiply_float(_SampleTexture2D_F60AE000_RGBA_0, _Property_51357914_Out_0, _Multiply_67569EC5_Out_2);
							surface.Albedo = (_SampleTexture2D_4F2A6AC5_RGBA_0.xyz);
							surface.Emission = (_Multiply_67569EC5_Out_2.xyz);
							surface.Alpha = 1;
							surface.AlphaClipThreshold = 0;
							return surface;
						}

						// --------------------------------------------------
						// Structs and Packing

						// Generated Type: Attributes
						struct Attributes
						{
							float3 positionOS : POSITION;
							float3 normalOS : NORMAL;
							float4 tangentOS : TANGENT;
							float4 uv0 : TEXCOORD0;
							float4 uv1 : TEXCOORD1;
							float4 uv2 : TEXCOORD2;
							#if UNITY_ANY_INSTANCING_ENABLED
							uint instanceID : INSTANCEID_SEMANTIC;
							#endif
						};

						// Generated Type: Varyings
						struct Varyings
						{
							float4 positionCS : SV_POSITION;
							float4 texCoord0;
							#if UNITY_ANY_INSTANCING_ENABLED
							uint instanceID : CUSTOM_INSTANCE_ID;
							#endif
							#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
							uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
							#endif
							#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
							uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
							#endif
							#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
							FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
							#endif
						};

						// Generated Type: PackedVaryings
						struct PackedVaryings
						{
							float4 positionCS : SV_POSITION;
							#if UNITY_ANY_INSTANCING_ENABLED
							uint instanceID : CUSTOM_INSTANCE_ID;
							#endif
							float4 interp00 : TEXCOORD0;
							#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
							uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
							#endif
							#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
							uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
							#endif
							#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
							FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
							#endif
						};

						// Packed Type: Varyings
						PackedVaryings PackVaryings(Varyings input)
						{
							PackedVaryings output = (PackedVaryings)0;
							output.positionCS = input.positionCS;
							output.interp00.xyzw = input.texCoord0;
							#if UNITY_ANY_INSTANCING_ENABLED
							output.instanceID = input.instanceID;
							#endif
							#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
							output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
							#endif
							#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
							output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
							#endif
							#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
							output.cullFace = input.cullFace;
							#endif
							return output;
						}

						// Unpacked Type: Varyings
						Varyings UnpackVaryings(PackedVaryings input)
						{
							Varyings output = (Varyings)0;
							output.positionCS = input.positionCS;
							output.texCoord0 = input.interp00.xyzw;
							#if UNITY_ANY_INSTANCING_ENABLED
							output.instanceID = input.instanceID;
							#endif
							#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
							output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
							#endif
							#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
							output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
							#endif
							#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
							output.cullFace = input.cullFace;
							#endif
							return output;
						}

						// --------------------------------------------------
						// Build Graph Inputs

						SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
						{
							SurfaceDescriptionInputs output;
							ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





							output.uv0 = input.texCoord0;
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
						#else
						#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
						#endif
						#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

							return output;
						}


						// --------------------------------------------------
						// Main

						#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
						#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

						ENDHLSL
					}

					Pass
					{
							// Name: <None>
							Tags
							{
								"LightMode" = "Universal2D"
							}

							// Render State
							Blend One Zero, One Zero
							Cull Off
							ZTest LEqual
							ZWrite On
							// ColorMask: <None>


							HLSLPROGRAM
							#pragma vertex vert
							#pragma fragment frag

							// Debug
							// <None>

							// --------------------------------------------------
							// Pass

							// Pragmas
							#pragma prefer_hlslcc gles
							#pragma exclude_renderers d3d11_9x
							#pragma target 2.0
							#pragma multi_compile_instancing

							// Keywords
							// PassKeywords: <None>
							// GraphKeywords: <None>

							// Defines
							#define _NORMALMAP 1
							#define _NORMAL_DROPOFF_TS 1
							#define ATTRIBUTES_NEED_NORMAL
							#define ATTRIBUTES_NEED_TANGENT
							#define ATTRIBUTES_NEED_TEXCOORD0
							#define VARYINGS_NEED_TEXCOORD0
							#define SHADERPASS_2D

							// Includes
							#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
							#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
							#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
							#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
							#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

							// --------------------------------------------------
							// Graph

							// Graph Properties
							CBUFFER_START(UnityPerMaterial)
							float _TilingX;
							float _TilingY;
							float _OffsetX;
							float _OffsetY;
							float _NormalStrength;
							float4 Color_6C763C74;
							float Vector1_3F115371;
							float Vector1_6029FDBF;
							CBUFFER_END
							TEXTURE2D(_Albedo); SAMPLER(sampler_Albedo); float4 _Albedo_TexelSize;
							TEXTURE2D(_NormalMapText); SAMPLER(sampler_NormalMapText); float4 _NormalMapText_TexelSize;
							TEXTURE2D(Texture2D_D5CF32D7); SAMPLER(samplerTexture2D_D5CF32D7); float4 Texture2D_D5CF32D7_TexelSize;
							TEXTURE2D(Texture2D_33E47080); SAMPLER(samplerTexture2D_33E47080); float4 Texture2D_33E47080_TexelSize;
							SAMPLER(_SampleTexture2D_4F2A6AC5_Sampler_3_Linear_Repeat);

							// Graph Functions

							void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
							{
								RGBA = float4(R, G, B, A);
								RGB = float3(R, G, B);
								RG = float2(R, G);
							}

							void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
							{
								Out = UV * Tiling + Offset;
							}

							// Graph Vertex
							// GraphVertex: <None>

							// Graph Pixel
							struct SurfaceDescriptionInputs
							{
								float4 uv0;
							};

							struct SurfaceDescription
							{
								float3 Albedo;
								float Alpha;
								float AlphaClipThreshold;
							};

							SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
							{
								SurfaceDescription surface = (SurfaceDescription)0;
								float _Property_6AB104D3_Out_0 = _TilingX;
								float _Property_D3E9070B_Out_0 = _TilingY;
								float4 _Combine_15D4F6D7_RGBA_4;
								float3 _Combine_15D4F6D7_RGB_5;
								float2 _Combine_15D4F6D7_RG_6;
								Unity_Combine_float(_Property_6AB104D3_Out_0, _Property_D3E9070B_Out_0, 0, 0, _Combine_15D4F6D7_RGBA_4, _Combine_15D4F6D7_RGB_5, _Combine_15D4F6D7_RG_6);
								float _Property_C5EE2266_Out_0 = _OffsetX;
								float _Property_3C3E4A46_Out_0 = _OffsetY;
								float4 _Combine_2E4A8FFE_RGBA_4;
								float3 _Combine_2E4A8FFE_RGB_5;
								float2 _Combine_2E4A8FFE_RG_6;
								Unity_Combine_float(_Property_C5EE2266_Out_0, _Property_3C3E4A46_Out_0, 0, 0, _Combine_2E4A8FFE_RGBA_4, _Combine_2E4A8FFE_RGB_5, _Combine_2E4A8FFE_RG_6);
								float2 _TilingAndOffset_B1CFF5A4_Out_3;
								Unity_TilingAndOffset_float(IN.uv0.xy, _Combine_15D4F6D7_RG_6, _Combine_2E4A8FFE_RG_6, _TilingAndOffset_B1CFF5A4_Out_3);
								float4 _SampleTexture2D_4F2A6AC5_RGBA_0 = SAMPLE_TEXTURE2D(_Albedo, sampler_Albedo, _TilingAndOffset_B1CFF5A4_Out_3);
								float _SampleTexture2D_4F2A6AC5_R_4 = _SampleTexture2D_4F2A6AC5_RGBA_0.r;
								float _SampleTexture2D_4F2A6AC5_G_5 = _SampleTexture2D_4F2A6AC5_RGBA_0.g;
								float _SampleTexture2D_4F2A6AC5_B_6 = _SampleTexture2D_4F2A6AC5_RGBA_0.b;
								float _SampleTexture2D_4F2A6AC5_A_7 = _SampleTexture2D_4F2A6AC5_RGBA_0.a;
								surface.Albedo = (_SampleTexture2D_4F2A6AC5_RGBA_0.xyz);
								surface.Alpha = 1;
								surface.AlphaClipThreshold = 0;
								return surface;
							}

							// --------------------------------------------------
							// Structs and Packing

							// Generated Type: Attributes
							struct Attributes
							{
								float3 positionOS : POSITION;
								float3 normalOS : NORMAL;
								float4 tangentOS : TANGENT;
								float4 uv0 : TEXCOORD0;
								#if UNITY_ANY_INSTANCING_ENABLED
								uint instanceID : INSTANCEID_SEMANTIC;
								#endif
							};

							// Generated Type: Varyings
							struct Varyings
							{
								float4 positionCS : SV_POSITION;
								float4 texCoord0;
								#if UNITY_ANY_INSTANCING_ENABLED
								uint instanceID : CUSTOM_INSTANCE_ID;
								#endif
								#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
								uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
								#endif
								#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
								uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
								#endif
								#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
								FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
								#endif
							};

							// Generated Type: PackedVaryings
							struct PackedVaryings
							{
								float4 positionCS : SV_POSITION;
								#if UNITY_ANY_INSTANCING_ENABLED
								uint instanceID : CUSTOM_INSTANCE_ID;
								#endif
								float4 interp00 : TEXCOORD0;
								#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
								uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
								#endif
								#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
								uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
								#endif
								#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
								FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
								#endif
							};

							// Packed Type: Varyings
							PackedVaryings PackVaryings(Varyings input)
							{
								PackedVaryings output = (PackedVaryings)0;
								output.positionCS = input.positionCS;
								output.interp00.xyzw = input.texCoord0;
								#if UNITY_ANY_INSTANCING_ENABLED
								output.instanceID = input.instanceID;
								#endif
								#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
								output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
								#endif
								#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
								output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
								#endif
								#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
								output.cullFace = input.cullFace;
								#endif
								return output;
							}

							// Unpacked Type: Varyings
							Varyings UnpackVaryings(PackedVaryings input)
							{
								Varyings output = (Varyings)0;
								output.positionCS = input.positionCS;
								output.texCoord0 = input.interp00.xyzw;
								#if UNITY_ANY_INSTANCING_ENABLED
								output.instanceID = input.instanceID;
								#endif
								#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
								output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
								#endif
								#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
								output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
								#endif
								#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
								output.cullFace = input.cullFace;
								#endif
								return output;
							}

							// --------------------------------------------------
							// Build Graph Inputs

							SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
							{
								SurfaceDescriptionInputs output;
								ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





								output.uv0 = input.texCoord0;
							#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
							#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
							#else
							#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
							#endif
							#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

								return output;
							}


							// --------------------------------------------------
							// Main

							#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
							#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

							ENDHLSL
						}

		}
			CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
								FallBack "Hidden/Shader Graph/FallbackError"
}
