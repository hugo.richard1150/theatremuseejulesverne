﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightCrossWater : MonoBehaviour
{
    public float depthZ;
    public float widthX;

    public GameObject lightPreset;
    public GameObject lightPreset2;

    public Vector3 pointDestination;
    public Vector3 pointDestination2;

    private void Start()
    {
        CalcuteNewPoint();

        StartCoroutine(ClockUpdatePoint());
    }

    private void CalcuteNewPoint()
    {
        pointDestination = new Vector3(Random.Range(widthX, -widthX), lightPreset.transform.position.y, Random.Range(depthZ, -depthZ));
        pointDestination2 = new Vector3(Random.Range(widthX, -widthX), lightPreset2.transform.position.y, Random.Range(depthZ, -depthZ));
    }

    private void Update()
    {
        lightPreset.transform.position = Vector3.Lerp(lightPreset.transform.position, pointDestination, Time.deltaTime / 10);
        lightPreset2.transform.position = Vector3.Lerp(lightPreset2.transform.position, pointDestination, Time.deltaTime / 15);
    }

    private IEnumerator ClockUpdatePoint()
    {
        yield return new WaitForSeconds(5f);
        CalcuteNewPoint();

        StartCoroutine(ClockUpdatePoint());
    }
}
