﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoriesManager : MonoBehaviour
{
    public Transform[] stories;
    public string[] storiesTitles;
    public Button[] buttonsList;
    [HideInInspector] public int storyIndexReference;

    [HideInInspector] public Animator anim;

    
    public GameObject leftCurtain;
    public GameObject rightCurtain;

    public enum CurtainsStatut { Open, Close }
    public CurtainsStatut curtainsStatut;

    public bool coroutineCanBeCalled;


    public Palette[] palettes;
    public GameObject paletteManagerGaOb;



    private void Start()
    {
        anim = GetComponent<Animator>();
        coroutineCanBeCalled = true;
        palettes = paletteManagerGaOb.GetComponentsInChildren<Palette>();
    }

    //Fonction appelée par les boutons
    public void LoadStory(int storyIndex)
    {
        if (coroutineCanBeCalled)
        {
            StartCoroutine(StoriesManagerEnum(storyIndex));
        }
    }

    private IEnumerator StoriesManagerEnum(int storyID)
    {
        coroutineCanBeCalled = false;

        //Les rideaux déjà fermés
        if (curtainsStatut == CurtainsStatut.Close)
        {
            UnloadStories();
            storyIndexReference = storyID;
            LoadStories();
            InitPalette(storiesTitles[storyID - 1]);

                yield return new WaitForSeconds(4);

            anim.SetBool("OpenCurtains", true);
            OpenCurtains();
            RotatingRouages.yes = true;
            RotatingRouages.sens = 1;
            yield return new WaitForSeconds(rightCurtain.GetComponent<Animation>()["Scene"].length/2);

            RotatingRouages.yes = false;
            Debug.Log("TIME : " + rightCurtain.GetComponent<Animation>()["Scene"].length);
            for (int i = 0; i < buttonsList.Length; i++)
            {
                buttonsList[i].interactable = true;
            }

            curtainsStatut = CurtainsStatut.Open;
            coroutineCanBeCalled = true;
        }
        //Les rideaux déjà ouverts
        else if (curtainsStatut == CurtainsStatut.Open)
        {
            anim.SetBool("OpenCurtains", false);
            CloseCurtains();
            RotatingRouages.yes = true;
            RotatingRouages.sens = -1;
            yield return new WaitForSeconds(rightCurtain.GetComponent<Animation>()["Scene_Reversed"].length / 2);

            UnloadStories();
            RotatingRouages.yes = false;
            storyIndexReference = storyID;

            curtainsStatut = CurtainsStatut.Close;
            coroutineCanBeCalled = true;

            StartCoroutine(StoriesManagerEnum(storyIndexReference));
        }
    }

    private void OpenCurtains()
    {
        ///Gestion de l'animation des rideaux
        //Ouverture du rideau de gauche
        Animation leftCurtainAnim = leftCurtain.GetComponent<Animation>();
        leftCurtainAnim["Scene"].speed = 2f;
        leftCurtainAnim.Play("Scene");
        leftCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);

        //Ouverture du rideau de droite
        Animation rightCurtainAnim = rightCurtain.GetComponent<Animation>();
        rightCurtainAnim["Scene"].speed = 2f;
        rightCurtainAnim.Play("Scene");
        rightCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);
    }

    private void CloseCurtains()
    {
        ///Gestion de l'animation des rideaux
        //Fermeture du rideau de gauche
        Animation leftCurtainAnim = leftCurtain.GetComponent<Animation>();
        leftCurtainAnim["Scene_Reversed"].speed = 2f;
        leftCurtainAnim.Play("Scene_Reversed");
        leftCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);

        //Fermeture du rideau de droite
        Animation rightCurtainAnim = rightCurtain.GetComponent<Animation>();
        rightCurtainAnim["Scene_Reversed"].speed = 2f;
        rightCurtainAnim.Play("Scene_Reversed");
        rightCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);
    }

    private void LoadStories()
    {
        //Activation des élèments graphiques de l'histoires selectionnée


        stories[storyIndexReference - 1].gameObject.SetActive(true);

        foreach (Transform item in stories[storyIndexReference - 1])
        {
            if (item != stories[storyIndexReference - 1])
            {
                item.gameObject.SetActive(true);
            }
        }

        //stories[storyIndexReference - 1].gameObject.SetActive(true);
        

        //Gestion des animations de l'histoire selectionnée
        anim.SetInteger("CurrentStory", storyIndexReference);
        anim.SetBool("PlayStory", true);
        anim.SetBool("TitleOpen", true);
        anim.SetBool("LoopStoryMain", false);
    }

    private void UnloadStories()
    {
        for (int i = 0; i < stories.Length; i++)
        {
            stories[i].gameObject.SetActive(false);
        }

    }

    //Fonctions appelées par des animations
    public void TitleOpen()
    {
        anim.SetBool("TitleOpen", false);
        anim.SetBool("OpenCurtains", true);
    }

    public void FinishedStory()
    {
        anim.SetBool("PlayStory", false);
    }

    public void MainStoryLoop()
    {
        anim.SetBool("LoopStoryMain", true);
    }

    public void InitPalette(string title)
    {
        foreach (var item in FindObjectsOfType<Palette>())
        {
            item.StopAllCoroutines();
            item.Refresh();
        }

        for (int i = 0; i < title.Length; i++)
        {
            palettes[i].StartCoroutine(palettes[i].RotatePalette(title[i]));
        }
    }

}
